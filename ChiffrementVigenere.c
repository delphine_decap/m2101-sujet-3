#include <stdio.h>
#include <wchar.h>
#include <locale.h>
#include <string.h>
#include "ChiffrementVigenere.h"

void chiffrerVigenere(wchar_t * clef, wchar_t * message){
        for (int i=0,j=0; i<wcslen(message);i++,j=(j+1)%wcslen(clef)){
                if (message[i]==' ' || message[i]=='\n') {
		} else {
                        message[i]=((message[i]-'a')+(clef[j]-'a'))%26+'a';
                }
        }
}

void dechiffrerVigenere(wchar_t * clef, wchar_t * message){
        for (int i=0,j=0; i<wcslen(message);i++,j=(j+1)%wcslen(clef)){
                if (message[i]==' ' || message[i]=='\n') {
		} else {
                        message[i]=((message[i]-'a')-(clef[j]-'a')+26)%26+'a';
                }
        }
}
