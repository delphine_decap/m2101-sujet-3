#include <stdio.h>
#include <wchar.h>
#include <locale.h>
#include <string.h>
#include "Conversions.h"

void convertirAccents(wchar_t * message){
        /*On convertira les accents uniquement sur les voyelles */
        wchar_t accentsA[] = L"áàâäãÁÀÂÄÃ";
        wchar_t accentsE[] = L"éèêëÉÈÊË";
        wchar_t accentsI[] = L"íìîïÍÌÎÏ";
        wchar_t accentsO[] = L"óòôöÓÒÔÖ";
        wchar_t accentsU[] = L"úùûüÚÙÛÜ";
        for(int i = 0; i < wcslen(message); i++){
                for(int j = 0; j < wcslen(accentsA); j++){
                        if(message[i] == accentsA[j]){
                                message[i] = 'a';
                        }
                }
                for(int j = 0; j < wcslen(accentsE); j++){
                        if(message[i] == accentsE[j]){
                                message[i] = 'e';
                        }
                }
                for(int j = 0; j < wcslen(accentsI); j++){
                        if(message[i] == accentsI[j]){
                                message[i] = 'i';
                        }
                }
                for(int j = 0; j < wcslen(accentsO); j++){
                        if(message[i] == accentsO[j]){
                                message[i] = 'o';
                        }
                }
                for(int j = 0; j < wcslen(accentsU); j++){
                        if(message[i] == accentsU[j]){
                                message[i] = 'u';
                        }
                }
        }
}

void convertirMinuscules(wchar_t * message){
        for(int i = 0; i < wcslen(message); i++){
                if(message[i] >= 'A' && message[i] <= 'Z'){
                        message[i] += 'a' - 'A';
                }
        }
}