#include <stdio.h>
#include <wchar.h>
#include <locale.h>
#include <string.h>
#include "ChiffrementCesar.h"

void chiffrerCesar(int clef, wchar_t * message){
	for(int i=0; i<wcslen(message); i++) {
		if (message[i]==' ' || message[i]=='\n') {
		} else {
			message[i]=((message[i]-'a')+clef)%26+'a';
		}
	}
}

void dechiffrerCesar(int clef, wchar_t * message){
	clef=clef%26;
        for(int i=0; i<wcslen(message); i++) {
		if (message[i]==' ' || message[i]=='\n') {
		} else {
			message[i]=((message[i]-'a')-clef+26)%26+'a';
		}
	}
}