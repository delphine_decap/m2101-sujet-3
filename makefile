GCC = gcc
SOURCES = $(wildcard *.c)
BINAIRES = $(patsubst %.c,%.o,${SOURCES})

all: main

%.o: %.c%.h
	${GCC} -c $<

main: ${BINAIRES}
	${GCC} $^ -o $@

clean: main
	rm main
	rm *.o