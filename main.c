#include <stdio.h>
#include <wchar.h>
#include <locale.h>
#include <string.h>
#include <stdlib.h>
#include "ChiffrementCesar.h"
#include "ChiffrementVigenere.h"
#include "Conversions.h"

#define TAILLE_MAX 501
#define TAILLE_MAX_NFIC 51

/*  fonction qui permet de vérifier si un charactere est alphanumérique ou non
        renvoie 1 si il l'est
        sinon renvoie 0
*/
int verifierAlphanumerique(wchar_t * message);

/*   fonction qui vide le buffer des saisies */
void viderBuffer();

void main() {
	// création des variables pour la suite du programme
	wchar_t choix[] = L"N";
 	wchar_t message[TAILLE_MAX];
	int chiffrement;
	int chiffreOuDechiffre;

	//On informe l’utilisateur des « modalités » 
	wprintf(L"Vos clefs et message a decoder doivent faire au maximum 500 caracteres (espaces compris)\n");
	// On demande à l’utilisateur d’entrer son message
	while(choix[0] != 'o'){
                wprintf(L"Quelle est le message a traiter ? tapez ; pour arreter la saisie\n");
                int i=0;
                message[i]=getchar();
                while (message[i]!=';'){
                        i++;
                        message[i]=getchar();
                }
                message[i]='\0';
		viderBuffer();
                     
	        /*On vérifie que la chaîne est alphanumérique, si elle ne l’est pas, on demande a l’utilisateur de re rentrer une chaine */
	        while(verifierAlphanumerique(message) == 0){
		wprintf(L"Le format de votre message n’est pas valide, veuillez le retaper :\n");
                        i=0;
                        message[i]=getchar();
                        while (message[i]!=';'){
                                i++;
                                message[i]=getchar();
                        }
                        message[i]='\0';
		        viderBuffer();
	        } 
                wprintf(L"%ls",message);       
	        /*On modifie le message pour enlever les éventuels accents*/
	        convertirAccents(message);

	        /*On modifie le message pour le mettre tout en minuscule*/
	        convertirMinuscules(message);
                
	        /*On vérifie que l'utilisateur à rentré le bon message*/
	        wprintf(L"Votre message est-il bien : (message sans majuscules ni accents)? \n%ls  \n(tapez o ou O si oui sinon tapez n’importe quoi d’autre)\n",message);
	        *choix=getchar();
	        viderBuffer();
	        convertirMinuscules(choix);
        }

	//On va demander à l’utilisateur si il souhaite chiffrer ou déchiffre son message
	wprintf(L"Voulez vous chiffrer ou dechiffrer le message (tapez 1 ou 2)?\n");
	wprintf(L"1.   Chiffrer\n");
	wprintf(L"2.   Dechiffrer\n");
                
	/*On enregistre le choix de l'utilisateur*/
	chiffreOuDechiffre=getchar()-'0';
        viderBuffer();
	/*On verifie que le choix de l'utilisateur soit correct, si il ne l’est pas, on recommence*/
        while(chiffreOuDechiffre != 1 && chiffreOuDechiffre != 2){
		wprintf(L"Il semblerait que vous ayez rentre un choix invalide, veuillez saisir un choix valide.\n");
		wprintf(L"1.   Chiffrer\n");
		wprintf(L"2.   Dechiffrer\n");
		chiffreOuDechiffre=getchar()-'0';
                viderBuffer();
	}
	
	/*On demande à l'utilisateur quel chiffrement il veut utiliser*/
	wprintf(L"Quel chiffrement voulez vous utiliser (tapez 1 ou 2)?\n");
	wprintf(L"1.   Vigenere\n");
	wprintf(L"2.   Cesar\n");
                
	/*On enregistre le choix de l'utilisateur*/
	chiffrement=getchar()-'0';
        viderBuffer();
	/*On verifie que le choix de l'utilisateur soit correct, si il ne l’est pas, on recommence*/
        while(chiffrement != 1 && chiffrement != 2){
		wprintf(L"Il semblerait que vous ayez rentre un choix invalide, veuillez saisir un choix valide.\n");
		wprintf(L"1.   Vigenere\n");
		wprintf(L"2.   Cesar\n");
		chiffrement=getchar()-'0';
                viderBuffer();
	}


        /*En fonction de l’algorithme choisit et du fait de chiffrer ou non, on demande la clef et effectue la demande*/
        switch(chiffreOuDechiffre){
                case 1 : ;
                        switch(chiffrement){
                                case 1: ; /*Chiffre de Vigenère*/
                                        wchar_t clef[TAILLE_MAX];
                                        wprintf(L"Votre clef doit etre une chaine de caracteres :");
                                        int i=0;
                                        clef[i]=getchar();
                                        while (clef[i]!='\n'){
                                                i++;
                                                clef[i]=getchar();
                                        }
                                        clef[i]='\0';
                                        chiffrerVigenere(clef,message);
                                        wprintf(L"Votre message chiffre: \n%ls\n",message);
                                        break;
                                case 2: ; /*Chiffre de César*/
                                        char clefChaine[TAILLE_MAX];
                                        wprintf(L"Votre clef doit etre un entier :");
                                        i=0;
                                        clefChaine[i]=getchar();
                                        while (clefChaine[i]!='\n'){
                                                i++;
                                                clefChaine[i]=getchar();
                                        }
                                        int cle=atoi(clefChaine);
                                        chiffrerCesar(cle,message);
                                        wprintf(L"Votre message chiffre : \n%ls\n", message);
                                        break;
                                default:
                                        printf("Erreur dans les choix\n");
                                        break;
                        }
                        break;
                case 2 : ;
                        switch(chiffrement){
                                case 1: ; /*Chiffre de Vigenère*/
                                        wchar_t clef[TAILLE_MAX];
                                        wprintf(L"Votre clef doit etre une chaine de caracteres :");
                                        int i=0;
                                        clef[i]=getchar();
                                        while (clef[i]!='\n'){
                                                i++;
                                                clef[i]=getchar();
                                        }
                                        clef[i]='\0';
                                        dechiffrerVigenere(clef,message);
                                        wprintf(L"Votre message dechiffre : \n%ls\n",message);
                                        break;
                                case 2: ; /*Chiffre de César*/
                                        char clefChaine[TAILLE_MAX];
                                        wprintf(L"Votre clef doit etre un entier :");
                                        i=0;
                                        clefChaine[i]=getchar();
                                        while (clefChaine[i]!='\n'){
                                                i++;
                                                clefChaine[i]=getchar();
                                        }
                                        int cle=atoi(clefChaine);
                                        dechiffrerCesar(cle,message);
                                        wprintf(L"Votre message dechiffre : \n%ls\n", message);
                                        break;
                                default:
                                        printf("Erreur dans les choix\n");
                                        break;
                        }
                        break;
                default : 
                        wprintf(L"Erreur dans les choix\n");
                        break;
        }
}

int verifierAlphanumerique(wchar_t * message){
        wchar_t characteresSpeciaux[] = L"><#'()[]\\{}/:;.?,!§%*µ$£¤^¨°+=~²@";
        for(int i = 0; i < wcslen(message); i++){
		for(int j = 0; j < wcslen(characteresSpeciaux); j++){
                        if(message[i] == characteresSpeciaux[j]){
                                return 0;
                        }
                }
        }
        return 1;
}


void viderBuffer(){
    int c;
    while (c != '\n' && c != EOF)
    {
        c = getchar();
    }
}
