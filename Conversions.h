/*  procedure qui enlève tous les accents d'une chaîne de caractères 
		modifie la chaine en remplacant les accents par des lettres non accentuées
*/
void convertirAccents(wchar_t * message);

/*   procedure qui convertit les majuscules en minuscule*/
void convertirMinuscules(wchar_t * message);