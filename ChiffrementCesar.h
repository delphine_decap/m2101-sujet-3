/*       procedure qui créé un message codée par la clé de césar
         prend en entrée la clé en integer et le message à coder en wchar_t
         transforme le message en message codé
*/
void chiffrerCesar(int clef, wchar_t * message);


/* procedure qui décode un message chiffré par la clef de César
        prend en entrée besoin de la clef utilisé pour coder le message (int) et le message codé en wchar_t
        transforme le message
*/
void dechiffrerCesar(int clef, wchar_t * message);
