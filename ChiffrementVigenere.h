/*       procedure qui créé un message codée par la clé de vigenère
         prend en entrée la clé en wchar_t et le message à coder en wchar_t
         transforme le message en message codé
*/
void chiffrerVigenere(wchar_t * clef, wchar_t * message);


/*       procedure qui décode un message codée par la clé de vigenère
         prend en entrée la clé en wchar_t et le message à décoder en wchar_t
         transforme le message codé en message clair
*/
void dechiffrerVigenere(wchar_t * clef, wchar_t * message);
